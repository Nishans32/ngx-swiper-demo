import { ViewChild, Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { SwiperTestService } from './swiper-test.service';
import { RestResponse, Customer } from './swiper-test.model';

import { SwiperComponent, SwiperDirective, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import {ViewEncapsulation} from '@angular/core';
import { ComponentRef } from '@angular/core/src/linker/component_factory';

@Component({
  selector: 'app-swiper-test',
  templateUrl: './swiper-test.component.html',
  styleUrls: ['./swiper-test.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SwiperTestComponent implements OnInit, AfterViewChecked {


responseObj:RestResponse;
internalDataSource : Customer[];

pagingIndex: number = 0;

pageSize : number = 7;

pagingIndexChanged: boolean = false;

changingArray :boolean = false;

constructor(private _service : SwiperTestService) { 
}

  ngOnInit() {
    this.responseObj = new RestResponse();

    this._service.getCountries("Mock")
    .subscribe(t => {
      this.getData(t);
    });
  }

  ngAfterViewInit(){

  }

  ngAfterViewChecked(): void {
    if(this.pagingIndexChanged){
      if(this.componentRef){
        this.increaseIndex(this.componentRef.directiveRef);
      }
      this.pagingIndexChanged = false;
    }
  }

  increaseIndex(directive : SwiperDirective): void {
    setTimeout(() => directive.setIndex(0, 0, true) );
  }

  getData(t : RestResponse): void {
    this.responseObj.value = t.value;
    this.populateDataArray(0);
  }

  public show: boolean = true;

  public type: string = 'component';

  public config: SwiperConfigInterface = {
    scrollbar: null,
    direction: 'horizontal',
    slidesPerView: 1,
    scrollbarHide: true,
    keyboardControl: true,
    mousewheelControl: true,
    scrollbarDraggable: true,
    scrollbarSnapOnRelease: true,
    pagination: '.swiper-pagination',
    paginationClickable: true
  };

  @ViewChild(SwiperComponent) componentRef: SwiperComponent;
  @ViewChild(SwiperDirective) directiveRef: SwiperDirective;


  onIndexChange(index: number) {

    if((index +1) % 7 == 0){
      this.populateDataArray(index);
    }
  }

  populateDataArray(index : number):void{

    console.log(this.pagingIndex);


    var start = this.pagingIndex ;
    var end = this.pagingIndex + 6;

  

    this.internalDataSource =[];

    this.changingArray = true;
    
    for(var i = start; i<=end; i++)
    {
      this.internalDataSource.push(this.responseObj.value[i])
    }
    
    this.changingArray = false;

    this.pagingIndex += this.pagingIndex + 6;

      this.pagingIndexChanged = true;
  }

}
