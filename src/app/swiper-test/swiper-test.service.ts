import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RestResponse, Customer } from "./swiper-test.model";

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs'
import 'rxjs/add/operator/do';

@Injectable()
export class SwiperTestService{

public environment: string= 'Dev';

constructor(private _http: HttpClient) {

}

getCountries(mode : string) : Observable<RestResponse>{
    if(mode == "Dev"){
        return this._http.get<RestResponse>("http://services.odata.org/V3/Northwind/Northwind.svc/Customers?$select=ContactName")
            .do(data => console.log(JSON.stringify(data)));
    }
    else{
        return this.getMockData();
    }
}

getMockData():Observable<RestResponse> {

    var restResponse : RestResponse = new RestResponse();
    
    let client1 : Customer = this.getNewCustomer("a");
    let client2 : Customer = this.getNewCustomer("b");
    let client3 : Customer = this.getNewCustomer("c");
    let client4 : Customer = this.getNewCustomer("d");
    let client5 : Customer = this.getNewCustomer("e");
    let client6 : Customer = this.getNewCustomer("f");
    let client7 : Customer = this.getNewCustomer("g");
    let client8 : Customer = this.getNewCustomer("h");
    let client9 : Customer = this.getNewCustomer("i");
    let client10 : Customer = this.getNewCustomer("j");
    let client11 : Customer = this.getNewCustomer("k");
    let client12 : Customer = this.getNewCustomer("L");
    let client13 : Customer = this.getNewCustomer("m");
    let client14 : Customer = this.getNewCustomer("n");
    let client15 : Customer = this.getNewCustomer("o");
    let client16 : Customer = this.getNewCustomer("p");


    var restResponse : RestResponse = new RestResponse();
    restResponse.value = [client1, client2, client3, client4, client5, client6, client7, client8, client9, client10, client11, client12, client13, client14, client15, client16 ]

    return new Observable<RestResponse>((observer) => {

    // observable execution
    observer.next(restResponse)
    observer.complete()
})

}

    getNewCustomer(name : string): Customer{
        return  {  ContactName : name }
    }
}