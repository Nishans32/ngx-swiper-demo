import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwiperTestComponent } from './swiper-test.component';
import { SwiperTestService } from './swiper-test.service';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';



const SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
  keyboardControl: true
};


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SwiperModule.forRoot(SWIPER_CONFIG)
  ],
  declarations: [SwiperTestComponent],
  exports: [SwiperTestComponent], 
  providers: [SwiperTestService]
})
export class SwiperTestModule { }
